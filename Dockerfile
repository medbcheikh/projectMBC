FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish GitlabProject.csproj -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
<<<<<<< HEAD
ENTRYPOINT ["dotnet", "GitlabProject.dll"]
=======
ENTRYPOINT ["dotnet", "GitlabProject.dll"]
>>>>>>> 055bb8178b2a14de607b243b0ee8bae3acf00c29
